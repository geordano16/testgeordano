﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGeordano
{
    public partial class Form1 : Form
    {
        public SqlConnection ConnectionString { get; set; } = new SqlConnection(@"Server =(localdb)\mssqllocaldb;Initial Catalog=Prueba;Integrated Security=True");
        public Form1()
        {
            InitializeComponent();
        }
        public List<int> Billetes { get; set; }


        private void btnTableMultiplication_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int number = (int)numericUpDown1.Value;

            for (int j = 1; j <= number; j++)
            {
                for (int i = 1; i <= 12; i++)
                {
                    listBox1.Items.Add($"{j} x {i} ={i * j}");
                }

            }
        }

        private void btnSec_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
        public static int Serie(int numero)
        {
            if (numero <= 1)
            {
                return numero;
            }
            else
            {
                return Serie(numero - 1) + Serie(numero - 2);
            }
        }
        private void btnSec_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int n = (int)numericUpDown1.Value;

            for (int i = 0; i < n; i++)
            {
                Console.Write(Serie(i));
                listBox1.Items.Add($"{Serie(i)}");
            }
        }
        private void BtnOrden_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int numero = (int)numericUpDown1.Value;
            var list = new List<int>();
            var randor = new Random();
            for (int i = 0; i < numero; i++)
            {
                list.Add(i);
            }
            do
            {
                int randorNumer = randor.Next(1, numero);
                if (list.Contains(randorNumer))
                {
                    list.Remove(randorNumer);
                    listBox1.Items.Add($"{randorNumer}");
                }

            } while (list.Count>=0);
            

        }

        private void btnDet_Click(object sender, EventArgs e)
        {

        }

        private void btnDet_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int cantidad = (int)numericUpDown1.Value;
            var billetes = new List<int>() {
                2000,
                1000,
                500,
                200,
                100,
                50,
                25,
                20,
                10,
                5,
                1
            };
            listBox1.Items.Add("Cantidad x billete");
            foreach (var billete in billetes)
            {
                if (cantidad == 0) break;
                var resultado = (int)cantidad / billete;
                if (resultado <= 0) continue;
                var totalRestar = resultado * billete;

                cantidad -= totalRestar;
                listBox1.Items.Add($"{resultado} X {billete} ={totalRestar}");

            }
        }

        private void toRigth_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Debe seleccionar un elemento de la lista de la izquierda");
                return;

            }
            var index = listBox1.SelectedIndex;
            listBox2.Items.Add(listBox1.Items[index]);
            listBox1.Items.Remove(listBox1.Items[index]);
            listBox1.Refresh();
        }

        private void btnAllToRigth_Click(object sender, EventArgs e)
        {
            var lista = listBox1.Items;
            listBox2.Items.AddRange(lista);
            listBox1.Items.Clear();
        }

        private void toLeft_Click(object sender, EventArgs e)
        {
            var index = listBox2.SelectedIndex;
            if (listBox1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Debe seleccionar un elemento de la lista de la derecha");
                return;

            }
            listBox1.Items.Add(listBox2.Items[index]);
            listBox2.Items.Remove(listBox2.Items[index]);
            listBox1.Refresh();
        }

        private void btnAllToLeft_Click(object sender, EventArgs e)
        {
            var lista = listBox2.Items;
            listBox1.Items.AddRange(lista);
            listBox2.Items.Clear();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                var selectTotalVentasxArticulosYBeneficio =
                    "SELECT a.[Codigo],a.[descripcion],a.[Cantidad],(a.cantidad* a.costo)as Costo, CAST(SUM(vd.cantidad *vd.precio) AS DECIMAL(18,2)) as Ventas, CAST (Sum((vd.cantidad *vd.precio)- (vd.costo * vd.cantidad))AS DECIMAL(18,2))as Beneficio FROM [Prueba].[dbo].[Articulos] a INNER JOIN [Prueba].[dbo].[VentasDetalle] vd ON a.id_articulo = vd.id_articulo GROUP BY  a.id_articulo,a.codigo, a.descripcion, a.cantidad, a.costo,a.precio";

                var selectTopVentasMasGrande =
                    "SELECT  TOP 10 a.[Codigo] as Articulo, v.[Fecha], v.[NumeroTransaccion], dv.[Cantidad],CAST(dv.[Precio] AS DECIMAL(18,2)) AS Precio,CAST((dv.cantidad * dv.precio) AS DECIMAL(18,2))as Total FROM [Prueba].[dbo].[VentasDetalle] dv INNER JOIN [Prueba].[dbo].[Ventas] v ON dv.NumeroTransaccion = v.NumeroTransaccion INNER JOIN [Prueba].[dbo].[Articulos] a ON a.id_articulo = dv.id_articulo GROUP BY  a.codigo, v.fecha, v.NumeroTransaccion, dv.cantidad, dv.precio ORDER BY Total desc";
                ConnectionString.Open();
                var dataadapter1 = new SqlDataAdapter(selectTotalVentasxArticulosYBeneficio, ConnectionString);
                var dataadapter2 = new SqlDataAdapter(selectTopVentasMasGrande, ConnectionString);
                var dt1 = new DataTable();
                var dt2 = new DataTable();
                dataadapter1.Fill(dt1);
                dataadapter2.Fill(dt2);
                dtGTotalVentasxArticulosYBenefici.DataSource = dt1;
                dtGTopVentasMasGrande.DataSource = dt2;

                ConnectionString.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Error:{exception}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
        }
    }

}
