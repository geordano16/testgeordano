﻿namespace TestGeordano
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDet = new System.Windows.Forms.Button();
            this.BtnOrden = new System.Windows.Forms.Button();
            this.btnSec = new System.Windows.Forms.Button();
            this.btnTableMultiplication = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnAllToLeft = new System.Windows.Forms.Button();
            this.btnAllToRigth = new System.Windows.Forms.Button();
            this.toLeft = new System.Windows.Forms.Button();
            this.toRigth = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtGTotalVentasxArticulosYBenefici = new System.Windows.Forms.DataGridView();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtGTopVentasMasGrande = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGTotalVentasxArticulosYBenefici)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGTopVentasMasGrande)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(786, 567);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnDet);
            this.tabPage1.Controls.Add(this.BtnOrden);
            this.tabPage1.Controls.Add(this.btnSec);
            this.tabPage1.Controls.Add(this.btnTableMultiplication);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.btnAllToLeft);
            this.tabPage1.Controls.Add(this.btnAllToRigth);
            this.tabPage1.Controls.Add(this.toLeft);
            this.tabPage1.Controls.Add(this.toRigth);
            this.tabPage1.Controls.Add(this.listBox2);
            this.tabPage1.Controls.Add(this.listBox1);
            this.tabPage1.Controls.Add(this.numericUpDown1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(778, 541);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnDet
            // 
            this.btnDet.Location = new System.Drawing.Point(635, 235);
            this.btnDet.Name = "btnDet";
            this.btnDet.Size = new System.Drawing.Size(120, 23);
            this.btnDet.TabIndex = 12;
            this.btnDet.Text = "Desgloce billetes";
            this.btnDet.UseVisualStyleBackColor = true;
            this.btnDet.Click += new System.EventHandler(this.btnDet_Click_1);
            // 
            // BtnOrden
            // 
            this.BtnOrden.Location = new System.Drawing.Point(635, 183);
            this.BtnOrden.Name = "BtnOrden";
            this.BtnOrden.Size = new System.Drawing.Size(120, 23);
            this.BtnOrden.TabIndex = 11;
            this.BtnOrden.Text = "Orden alearorio";
            this.BtnOrden.UseVisualStyleBackColor = true;
            this.BtnOrden.Click += new System.EventHandler(this.BtnOrden_Click);
            // 
            // btnSec
            // 
            this.btnSec.Location = new System.Drawing.Point(635, 131);
            this.btnSec.Name = "btnSec";
            this.btnSec.Size = new System.Drawing.Size(120, 23);
            this.btnSec.TabIndex = 10;
            this.btnSec.Text = "Secuencia";
            this.btnSec.UseVisualStyleBackColor = true;
            this.btnSec.Click += new System.EventHandler(this.btnSec_Click_1);
            // 
            // btnTableMultiplication
            // 
            this.btnTableMultiplication.Location = new System.Drawing.Point(635, 85);
            this.btnTableMultiplication.Name = "btnTableMultiplication";
            this.btnTableMultiplication.Size = new System.Drawing.Size(120, 23);
            this.btnTableMultiplication.TabIndex = 9;
            this.btnTableMultiplication.Text = "Tabla Multiplicar";
            this.btnTableMultiplication.UseVisualStyleBackColor = true;
            this.btnTableMultiplication.Click += new System.EventHandler(this.btnTableMultiplication_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(275, 287);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 8;
            this.button5.Text = "Limpiar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnAllToLeft
            // 
            this.btnAllToLeft.Location = new System.Drawing.Point(275, 235);
            this.btnAllToLeft.Name = "btnAllToLeft";
            this.btnAllToLeft.Size = new System.Drawing.Size(75, 23);
            this.btnAllToLeft.TabIndex = 7;
            this.btnAllToLeft.Text = "<<";
            this.btnAllToLeft.UseVisualStyleBackColor = true;
            this.btnAllToLeft.Click += new System.EventHandler(this.btnAllToLeft_Click);
            // 
            // btnAllToRigth
            // 
            this.btnAllToRigth.Location = new System.Drawing.Point(275, 183);
            this.btnAllToRigth.Name = "btnAllToRigth";
            this.btnAllToRigth.Size = new System.Drawing.Size(75, 23);
            this.btnAllToRigth.TabIndex = 6;
            this.btnAllToRigth.Text = ">>";
            this.btnAllToRigth.UseVisualStyleBackColor = true;
            this.btnAllToRigth.Click += new System.EventHandler(this.btnAllToRigth_Click);
            // 
            // toLeft
            // 
            this.toLeft.Location = new System.Drawing.Point(275, 131);
            this.toLeft.Name = "toLeft";
            this.toLeft.Size = new System.Drawing.Size(75, 23);
            this.toLeft.TabIndex = 5;
            this.toLeft.Text = "<";
            this.toLeft.UseVisualStyleBackColor = true;
            this.toLeft.Click += new System.EventHandler(this.toLeft_Click);
            // 
            // toRigth
            // 
            this.toRigth.Location = new System.Drawing.Point(275, 74);
            this.toRigth.Name = "toRigth";
            this.toRigth.Size = new System.Drawing.Size(75, 23);
            this.toRigth.TabIndex = 4;
            this.toRigth.Text = ">";
            this.toRigth.UseVisualStyleBackColor = true;
            this.toRigth.Click += new System.EventHandler(this.toRigth_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(382, 47);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(223, 459);
            this.listBox2.TabIndex = 3;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(22, 48);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(223, 459);
            this.listBox1.TabIndex = 2;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(635, 47);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 1;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dtGTopVentasMasGrande);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.dtGTotalVentasxArticulosYBenefici);
            this.tabPage2.Controls.Add(this.btnGenerar);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(778, 541);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(582, 511);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "DB:Prueba";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(335, 511);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Segurdad: Windows auntentication";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 511);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Servidor : (localdb)\\mssqllocaldb";
            // 
            // dtGTotalVentasxArticulosYBenefici
            // 
            this.dtGTotalVentasxArticulosYBenefici.AllowUserToAddRows = false;
            this.dtGTotalVentasxArticulosYBenefici.AllowUserToDeleteRows = false;
            this.dtGTotalVentasxArticulosYBenefici.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtGTotalVentasxArticulosYBenefici.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtGTotalVentasxArticulosYBenefici.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtGTotalVentasxArticulosYBenefici.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtGTotalVentasxArticulosYBenefici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGTotalVentasxArticulosYBenefici.EnableHeadersVisualStyles = false;
            this.dtGTotalVentasxArticulosYBenefici.Location = new System.Drawing.Point(6, 46);
            this.dtGTotalVentasxArticulosYBenefici.Name = "dtGTotalVentasxArticulosYBenefici";
            this.dtGTotalVentasxArticulosYBenefici.ReadOnly = true;
            this.dtGTotalVentasxArticulosYBenefici.RowHeadersVisible = false;
            this.dtGTotalVentasxArticulosYBenefici.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGTotalVentasxArticulosYBenefici.Size = new System.Drawing.Size(766, 195);
            this.dtGTotalVentasxArticulosYBenefici.TabIndex = 4;
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(7, 506);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(75, 23);
            this.btnGenerar.TabIndex = 2;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.LightGray;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 266);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(765, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "LAS 10 VENTAS MAS GRANDES (TOAL)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.LightGray;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(765, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "TOTAL DE VENTAS X ARITCULO Y BENEFICIO";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtGTopVentasMasGrande
            // 
            this.dtGTopVentasMasGrande.AllowUserToAddRows = false;
            this.dtGTopVentasMasGrande.AllowUserToDeleteRows = false;
            this.dtGTopVentasMasGrande.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtGTopVentasMasGrande.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtGTopVentasMasGrande.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dtGTopVentasMasGrande.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtGTopVentasMasGrande.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGTopVentasMasGrande.EnableHeadersVisualStyles = false;
            this.dtGTopVentasMasGrande.Location = new System.Drawing.Point(7, 292);
            this.dtGTopVentasMasGrande.Name = "dtGTopVentasMasGrande";
            this.dtGTopVentasMasGrande.ReadOnly = true;
            this.dtGTopVentasMasGrande.RowHeadersVisible = false;
            this.dtGTopVentasMasGrande.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGTopVentasMasGrande.Size = new System.Drawing.Size(766, 195);
            this.dtGTopVentasMasGrande.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 586);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGTotalVentasxArticulosYBenefici)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGTopVentasMasGrande)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.TabPage tabPage2;
        private  System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnAllToRigth;
        private System.Windows.Forms.Button toLeft;
        private System.Windows.Forms.Button toRigth;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnAllToLeft;
        private System.Windows.Forms.Button btnDet;
        private System.Windows.Forms.Button BtnOrden;
        private System.Windows.Forms.Button btnSec;
        private System.Windows.Forms.Button btnTableMultiplication;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.DataGridView dtGTotalVentasxArticulosYBenefici;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dtGTopVentasMasGrande;
    }
}

